module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-mocha-test');

    grunt.initConfig({
        mochaTest: {
            options: { reporter: 'spec' },
            src: ['test/*.js']
        },
    });

    grunt.registerTask('test', ['mochaTest']);
};
