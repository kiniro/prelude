const assert = require('assert');
const prelude = require('../index');

describe('prelude', () => {
    describe('raw', () => {
        describe('new', () => {
            class C {
                constructor () {
                    this.args = Array.from(arguments);
                }
            };

            it('does not throw when constructor given', () => {
                assert.doesNotThrow(() => {
                    prelude.raw.new(function () {});
                }, x => x == "[new]: Not a constructor.");
            });

            it('throws when non-constructor given', () => {
                assert.throws(() => {
                    prelude.raw.new(() => {});
                }, x => x == "[new]: Not a constructor.");
            });

            it('accepts arguments correctly', () => {
                assert.deepStrictEqual(prelude.raw.new(C, 1, 2, 3).args
                                       [1,2,3]);
            });
        });
    });
});
