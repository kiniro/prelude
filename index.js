const isDefined = x => x[0] < 'u';

const prelude = module.exports = {
    raw: {
        /* Standard objects
           https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects
        */

        /* Global values */
        Infinity,
        NaN,
        undefined,
        null: null,

        /* Global functions */

        /* eval,
           uneval, */
        isFinite,
        isNaN,
        parseFloat,
        parseInt,
        decodeURI,
        decodeURIComponent,
        encodeURI,
        encodeURIComponent,
        // Deprecated. Need to check.
        escape: isDefined(typeof escape) ? escape : undefined,
        unescape: isDefined(typeof unescape) ? unescape : undefined,

        /* Fundamental objects */

        Object,
        Function,
        Boolean,
        Symbol,
        Error,
        EvalError,
        /*
           InternalError, // not available in node.js
        */
        RangeError,
        ReferenceError,
        SyntaxError,
        TypeError,
        URIError,

        /* Numbers and dates */
        Number,
        Math,
        Date,

        /* Text processing */
        String,
        RegExp,

        /* Indexed collections */

        Array,
        Int8Array,
        Uint8Array,
        Uint8ClampedArray,
        Int16Array,
        Uint16Array,
        Int32Array,
        Uint32Array,
        Float32Array,
        Float64Array,

        /* Keyed collections */
        Map,
        Set,
        WeakMap,
        WeakSet,

        /* Structured data */
        ArrayBuffer,
        /*
           SharedArrayBuffer,
           Atomics, // not available in node.js
        */
        DataView,
        JSON,

        Promise,
        /* Generator, */
        GeneratorFunction: Object.getPrototypeOf(function*(){}).constructor,
        AsyncFunction: Object.getPrototypeOf(async function(){}).constructor,

        /* Reflection */
        Reflect,
        Proxy,

        /* Other */
        console,
        log: console.log,

        alert: isDefined(typeof alert) ? alert : undefined,
        prompt: isDefined(typeof prompt) ? prompt : undefined,
        confirm: isDefined(typeof confirm) ? confirm : undefined,

        clearImmediate: isDefined(typeof clearImmediate) ? clearImmediate : undefined,
        clearInterval,
        clearTimeout,
        setImmediate: isDefined(typeof setImmediate) ? setImmediate : undefined,
        setInterval,
        setTimeout,

        /*
           Intl,
           WebAssembly,
        */

        /* logic */
        'not': x => !x,
        'and': function () {
            return [].every.call(arguments, x => x);
        },
        'or': function () {
            return [].some.call(arguments, x => x);
        },

        true: true,
        false: false,

        /* arithmetics */
        '===': (a, b) => a === b,
        '==': (a, b) => a == b,
        '>=': (a, b) => a >= b,
        '<=': (a, b) => a <= b,
        '>': (a, b) => a > b,
        '<': (a, b) => a < b,
        '+': function () {
            return [].reduce.call(arguments, (x, y) => x + y, 0);
        },
        '-': function (a, b) {
            if (arguments.length == 1) {
                return - a;
            }
            return a - b;
        },
        '*': function () {
            return [].reduce.call(arguments, (x, y) => x * y, 1);
        },
        '/': (a, b) => a / b,

        sleep: function (x) {
            return new Promise(resolve => setTimeout(() => resolve(x), x));
        },

        nil: [],

        new: function (func) {
            if (typeof func.prototype !== 'undefined') {
                return new func(...Array.from(arguments).slice(1));
            } else {
                throw "[new]: Not a constructor.";
            }
        }
    },
    lifted: {}
};
