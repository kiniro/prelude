![npm bundle size (minified)](https://img.shields.io/bundlephobia/min/@kiniro/prelude.svg)
![NpmLicense](https://img.shields.io/npm/l/@kiniro/prelude.svg)

Definitions for [@kiniro/lang](https://gitlab.com/kiniro/lang).

[gitlab](https://gitlab.com/kiniro/prelude) / [npm](https://www.npmjs.com/package/@kiniro/prelude)
